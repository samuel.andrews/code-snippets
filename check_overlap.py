import requests, json, time


def get_witnesses(address):

    url = requests.get("https://api.helium.io/v1/hotspots/" + address + "/witnesses/")
    witness_list = json.loads(url.text)
    witness_list = witness_list["data"]
    witnesses = []

    for witness in witness_list:
        #print(witness)
        witnesses.extend([witness["name"]])

    return witnesses


#two hotspots to check - I just put in two randomly for this example
hotspot1 = "112B4cZpKEGj36rwueoTx6KdL7LpSbdzKVgo5QswK4GavhgAuhhX" #only put the address in, not the name (refined taupe seal)
hotspot2 = "1129uA8j9EEMXTvXs5wKGxJyoGggdnHfu3dPrjEr1G9Y5Ypwc8vT" #only put the address in, not the name (bitter ocean terrier)

first_witnesses = get_witnesses(hotspot1)
time.sleep(.5)
second_witnesses = get_witnesses(hotspot2)

#print(first_witnesses)
#print("------------")
#print(second_witnesses)

overlap = 0

print("Overlapping witnesses: ")
for hotspot in first_witnesses:
    if hotspot in second_witnesses:
        print(hotspot)
        overlap = overlap + 1

percent = 0.0

if overlap != 0:
    percent = overlap / len(first_witnesses)

print("----------------------")
print("There are " + str(overlap) + " overlapping witnesses.")
print("First hotspot overlaps witnesses by " + str(percent*100.0) + "%")

if overlap != 0:
    percent = overlap / len(second_witnesses)

print("Second hotspot overlaps witnesses by " + str(percent*100.0) + "%")
